# Droplet

![Droplet logo](./icons/droplet-96.png 'Droplet logo')
Automatically theme your browser. Lightish or darkish style.

Droplet is a Firefox theming extension that provides light and dark modes using the nord color palette.
You can select light or dark mode, or you can set a schedule to have the mode change automatically based upon the time of day.
Changes are synchronized across your browser instances if you use Firefox Sync and have the 'Add-ons' option activated in the sync settings.

### Why did you create this extension?

A few reasons:

- I love the [nord colors](https://nordtheme.com), and I wanted to use a nord theme on my browser. There are a few out there but I was looking for something that was a little different than the ones I found.
- I wanted a theme that changes from light to dark automatically, using the nord colors. There are a few extensions that do this out there but no nord color ones.
- I wanted to learn some things:
  - asynchronous JavaScript
  - how to build browser extensions
  - CSSing experience
  - tiny application design
  - logo/icon design

### Permissions

Here are the permissions that Droplet uses:

- `theme` is used to update the browser theme
- `storage` is used to store the options that are selected
- `alarms` is used for the schedule feature that will automatically change the theme at selected times

### Thank you

Every project is built on top of work that was done before. Here are a few of the resources that I drew from in order to build this project.

#### Extensions

This project was inspired by looking at the work of a couple other browser extensions.

[Zen Fox](https://github.com/mr-islam/zen-fox)
This extension is the inspiration for the schedule-based theme update feature. Available for [Firefox](https://addons.mozilla.org/en-US/firefox/addon/zen-fox).

[Auto Tab Discard](https://add0n.com/tab-discard.html)
This extension discards (i.e. puts to sleep) inactive tabs in your browser. Very configurable. Available for [Chrome](https://chrome.google.com/webstore/detail/auto-tab-discard/jhnleheckmknfcgijgkadoemagpecfol) and [Firefox](https://addons.mozilla.org/en-US/firefox/addon/auto-tab-discard/).

[Tab Stash](https://josh-berry.github.io/tab-stash/)
This extension allows you to save links to your bookmarks, which synchronizes across instances if you use the sync feature. Very easy to use and very helpful! Available for [Firefox](https://addons.mozilla.org/en-US/firefox/addon/tab-stash/).

#### Fonts

Chris Ferdinandi is The Vanilla JS Guy and he shared a great article about [How to self-host Google fonts](https://gomakethings.com/how-to-self-host-google-fonts/). I appreciated the article, and the motivations behind it.
You can find more Vanilla JS awesomeness at Chris' site [Go Make Things](https://gomakethings.com) and you can find Chris on Twitter [@ChrisFerdinandi](https://twitter.com/ChrisFerdinandi).

#### Icons

The Feather icons are really great, and while I did not use them directly, I was inspired by them with this project. Check them out at [feathericons.com](https://feathericons.com/).

#### Styles

Stephanie Eckles has some super-helpful tutorials on her site [Modern CSS](https://moderncss.dev). She also has a reference site that has a ton of useful info at [SmolCSS](https://smolcss.dev/).
You can find her on Twitter [@5t3ph](https://twitter.com/5t3ph/);

#### Custom Controls

The double-range slider control was adapted from one that I found on [codingartistweb.com](https://codingartistweb.com/2021/06/double-range-slider-html-css-javascript/). I like this approach because it does not depend upon external libraries, just HTML, CSS, and JavaScript.

The light/dark mode toggle buttons were inspired by [a codepen](https://codepen.io/JiveDig/pen/jbdJXR?editors=1100) I found by Mike Hemberger ([@JiveDig](https://codepen.io/JiveDig)).
