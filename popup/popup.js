let KEYS = undefined;
const toggleLight = document.getElementById('toggleLight');
const toggleDark = document.getElementById('toggleDark');
const schedule = document.getElementById('schedule');
const start = document.getElementById('start');
const end = document.getElementById('end');
const startDisplay = document.getElementById('startDisplay');
const endDisplay = document.getElementById('endDisplay');
const startDarkDisplay = document.getElementById('startDarkDisplay');
const endDarkDisplay = document.getElementById('endDarkDisplay');
const minimumHourLength = 1;
const sliderTrack = document.querySelector('.slider-track');
const sliderMaxValue = start.max;
const infoButton = document.getElementById('info');

const infoClick = () => {
  browser.runtime
    .openOptionsPage()
    .then(() => {
      messageSend({
        type: KEYS.MESSAGE_TYPES.LOG,
        message: `popup.infoClick: the options page was opened`,
      });
    })
    .catch(() => {
      messageSend({
        type: KEYS.MESSAGE_TYPES.LOG,
        message: {
          text: `popup.infoClick: an error occurred while opening the options page`,
          isError: true,
        },
      });
    });
};

const startInput = () => {
  if (parseInt(end.value) - parseInt(start.value) <= minimumHourLength) {
    start.value = parseInt(end.value) - minimumHourLength;
  }
  startDisplay.textContent = valueMapToDisplay(start.value);
  endDarkDisplay.textContent = valueMapToDisplay(start.value);
  colorFill();
  lightHoursSave();
};

const endInput = () => {
  if (parseInt(end.value) - parseInt(start.value) <= minimumHourLength) {
    end.value = parseInt(start.value) + minimumHourLength;
  }
  endDisplay.textContent = valueMapToDisplay(end.value);
  startDarkDisplay.textContent = valueMapToDisplay(end.value);
  colorFill();
  lightHoursSave();
};

const lightHoursSave = () => {
  messageSend({
    type: KEYS.MESSAGE_TYPES.OPTION_SAVE,
    option: {
      lightHours: {
        start: start.value,
        end: end.value,
      },
    },
  });

  if (schedule.checked) {
    const mode = colorModeDefine();
    colorModeSet(mode);
    colorModeToggleStyle(mode);
  }
};

const colorFill = () => {
  percent1 = (start.value / sliderMaxValue) * 100;
  percent2 = (end.value / sliderMaxValue) * 100;
  sliderTrack.style.background = `linear-gradient(to right, #2e3440 ${percent1}% , #ebcb8b ${percent1}% , #ebcb8b ${percent2}%, #2e3440 ${percent2}%)`;
};

const valueMapToDisplay = (value) => {
  const number = +value;
  let display;

  switch (true) {
    case number === 0:
      display = '12:00';
      break;
    case number >= 1 && number <= 12:
      display = `${value}:00`;
      break;
    case number > 12:
      display = `${number - 12}:00`;
      break;
  }

  return (display += number < 12 ? ' am' : ' pm');
};

const colorModeToggleChange = (event) => {
  colorModeSet(event.target.value);
  colorModeToggleStyle(event.target.value);
};

const colorModeToggleStyle = (mode) => {
  const lightIcon = document.getElementById('lightIcon');
  const darkIcon = document.getElementById('darkIcon');

  if (mode === KEYS.MODE_LIGHT) {
    toggleLight.checked = true;
    lightIcon.src = '../icons/sun-fill.png';
    darkIcon.src = '../icons/moon.png';
  } else {
    toggleDark.checked = true;
    lightIcon.src = '../icons/sun.png';
    darkIcon.src = '../icons/moon-fill.png';
  }
};

const scheduleChange = () => {
  messageSend({
    type: KEYS.MESSAGE_TYPES.OPTION_SAVE,
    option: { scheduleIsSet: schedule.checked },
  });
  let mode = colorModeDefine();
  colorModeToggleStyle(mode);

  if (schedule.checked) {
    modeToggleDisable(true);
    colorModeSet(mode);
    messageSend({ type: KEYS.MESSAGE_TYPES.TIMER_SET, colorMode: mode });
  } else {
    modeToggleDisable(false);
    messageSend({ type: KEYS.MESSAGE_TYPES.TIMER_REMOVE });
  }
};

const modeToggleDisable = (shouldDisable) => {
  toggleLight.disabled = shouldDisable;
  toggleDark.disabled = shouldDisable;
};

const colorModeDefine = () => {
  const hourStart = start.value;
  const hourEnd = end.value;
  const hourCurrent = new Date().getHours();
  const mode =
    hourCurrent >= hourStart && hourCurrent < hourEnd
      ? KEYS.MODE_LIGHT
      : KEYS.MODE_DARK;
  return mode;
};

const colorModeSet = (mode) => {
  document.documentElement.setAttribute(KEYS.MODE_ATTRIBUTE, mode);

  messageSend({
    type: KEYS.MESSAGE_TYPES.MODE_SET,
    colorMode: mode,
  });
};

const messageSend = async (message) => {
  const response = await browser.runtime.sendMessage(message);
  return response;
};

const initialize = async () => {
  await messageSend({
    type: 'keysFetch',
  })
    .then((result) => {
      KEYS = result;
      messageSend({
        type: KEYS.MESSAGE_TYPES.LOG,
        message: `popup.initialize: KEYS fetched\n${JSON.stringify(
          KEYS,
          null,
          2,
        )}`,
      });
    })
    .catch((error) => {
      messageSend({
        type: 'log',
        message: `popup.initialize: ERROR fetching KEYS\n${error}`,
      });
    });

  await browser.storage.sync
    .get(null)
    .then((options) => {
      const { colorMode, scheduleIsSet, lightHours } = options;

      document.documentElement.setAttribute(KEYS.MODE_ATTRIBUTE, colorMode);
      if (colorMode === KEYS.MODE_LIGHT) {
        toggleLight.checked = true;
      } else if (colorMode === KEYS.MODE_DARK) {
        toggleDark.checked = true;
      }

      schedule.checked = scheduleIsSet;
      modeToggleDisable(scheduleIsSet);

      start.value = lightHours['start'];
      end.value = lightHours['end'];
      startInput();
      endInput();

      messageSend({
        type: KEYS.MESSAGE_TYPES.LOG,
        message: `popup.initialize: fetched saved values\n${JSON.stringify(
          options,
          null,
          2,
        )}`,
      });
    })
    .catch((error) => {
      messageSend({
        type: KEYS.MESSAGE_TYPES.LOG,
        message: {
          text: `popup.initialize: error fetching saved values\n${error}`,
          isError: true,
        },
      });
    });

  toggleLight.addEventListener('change', colorModeToggleChange);
  toggleDark.addEventListener('change', colorModeToggleChange);
  schedule.addEventListener('change', scheduleChange);
  start.addEventListener('input', startInput);
  end.addEventListener('input', endInput);
  infoButton.addEventListener('click', infoClick);
};

initialize();
