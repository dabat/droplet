let KEYS;

const modeApply = (mode) => {
  document.documentElement.setAttribute(KEYS.MODE_ATTRIBUTE, mode);
};

const messageSendAsync = async (message) => {
  const response = await browser.runtime.sendMessage(message);
  return response;
};

browser.runtime.onMessage.addListener((message) => {
  if (message.type === KEYS.MESSAGE_TYPES.MODE_SET) {
    modeApply(message.colorMode);
    messageSendAsync({
      type: KEYS.MESSAGE_TYPES.LOG,
      message: `history.onMessage: message received ${JSON.stringify(
        message,
        null,
        2,
      )}`,
    });
  }
});

const initializeAsync = async () => {
  await messageSendAsync({
    type: 'keysFetch',
  })
    .then((result) => {
      KEYS = result;
      messageSendAsync({
        type: KEYS.MESSAGE_TYPES.LOG,
        message: `history.initializeAsync: KEYS fetched\n${JSON.stringify(
          KEYS,
          null,
          2,
        )}`,
      });
    })
    .catch((error) => {
      messageSendAsync({
        type: 'log',
        message: {
          text: `history.initializeAsync: ERROR fetching KEYS\n${error}`,
          isError: true,
        },
      });
    });

  await browser.storage.sync
    .get(null)
    .then((options) => {
      const { colorMode } = options;

      document.documentElement.setAttribute(KEYS.MODE_ATTRIBUTE, colorMode);

      messageSendAsync({
        type: KEYS.MESSAGE_TYPES.LOG,
        message: `history.initializeAsync: fetched options\n${JSON.stringify(
          options,
          null,
          2,
        )}`,
      });
    })
    .catch((error) => {
      messageSendAsync({
        type: KEYS.MESSAGE_TYPES.LOG,
        message: {
          text: `history.initializeAsync: error fetching saved options\n${error}`,
          isError: true,
        },
      });
    });
};

initializeAsync();
