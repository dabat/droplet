let debug = false;

const log = async (message) => {
  if (!debug) {
    return;
  }

  if (typeof message === 'string') {
    console.log(message);
  } else {
    /* { text: 'something errorish', isError: true } */
    if (message.isError) {
      console.error(message.text);
    }
  }
};

const checkStorage = () => {
  browser.storage.sync
    .getBytesInUse(null)
    .then((bytes) => log(`checkStorage: bytes=${bytes}`));
  browser.storage.sync
    .get(null)
    .then((options) =>
      log(`checkStorage: options\n${JSON.stringify(options, null, 2)}`),
    );
};

const testTimer = async (timerTime) => {
  // FOR TESTING
  // const now = new Date();
  // const time = new Date().setHours(now.getHours(), now.getMinutes() + 5, 0);
  // testTimer(time);
  log(`testTimer: setting up`);
  debug = true;
  optionSave({ debugIsSet: debug });

  const now = new Date();
  const thisHour = now.getHours();
  optionSave({
    lightHours: {
      start: thisHour - 2,
      end: thisHour - 1,
    },
  });
  optionSave({ scheduleIsSet: true });

  modeSet('light');

  if (!timerTime) {
    const thisMinute = now.getMinutes();
    timerTime = new Date().setHours(thisHour, thisMinute + 2, 0);
  }

  await modeTimerRemove();
  await modeTimerSet(timerTime);

  const alarms = await browser.alarms.getAll();
  alarms.forEach((alarm) => {
    log(
      `testTimer: alarm '${alarm.name}' scheduled to fire at ${new Date(
        alarm.scheduledTime,
      )}`,
    );
  });
  log(`testTimer: all set`);
};

const optionFetch = async (keys) => {
  return Promise.resolve(
    await browser.storage.sync
      .get(keys)
      .then((results) => {
        log(
          `optionFetch: keys=\n${JSON.stringify(keys, null, 2)}\n
          results=\n${JSON.stringify(results, null, 2)}`,
        );
        return results;
      })
      .catch((error) => log({ text: error, isError: true })),
  );
};

const optionSave = async (option) => {
  await browser.storage.sync
    .set(option)
    .then(log(`optionSave: saved ${JSON.stringify(option, null, 2)}`))
    .catch((error) =>
      log({
        text: `optionSave: error occurred while saving option ${JSON.stringify(
          option,
          null,
          2,
        )}\n${error}`,
        isError: true,
      }),
    );
};

const modeSet = async (mode) => {
  browser.theme.update(themes[mode]);
  optionSave({ colorMode: mode });
  log(`modeSet: set mode to '${mode}'.`);
};

const modeApplySaved = () => {
  log(`modeApplySaved: alarm '${KEYS.MODE_ALARM}' fired`);
  optionFetch(KEYS.LIGHT_HOURS).then((option) => {
    const { lightHours } = option;
    const { start, end } = lightHours;
    const thisHour = new Date().getHours();
    const mode =
      start <= thisHour && thisHour < end ? KEYS.MODE_LIGHT : KEYS.MODE_DARK;
    modeSet(mode);
    log(
      `modeApplySaved: start=${start} end=${end} thishour=${thisHour} mode=${mode}`,
    );
  });
};

const modeTimerSet = async (when = 0) => {
  const alarm = await browser.alarms.get(KEYS.MODE_ALARM);

  if (!alarm) {
    if (when === 0) {
      const thisHour = new Date().getHours();
      when = new Date().setHours(thisHour + 1, 0, 0);
    }

    const alarmOptions = { when: when, periodInMinutes: 60 };
    browser.alarms.create(KEYS.MODE_ALARM, alarmOptions);

    log(
      `modeTimerSet: alarm '${
        KEYS.MODE_ALARM
      }' set with options: ${JSON.stringify(alarmOptions, null, 2)}`,
    );
  }

  if (!browser.alarms.onAlarm.hasListener(modeApplySaved)) {
    log(`modeTimerSet: adding listener`);
    browser.alarms.onAlarm.addListener(modeApplySaved);
  }
};

const modeTimerRemove = async () => {
  browser.alarms.clear(KEYS.MODE_ALARM).then((alarmWasCleared) => {
    if (alarmWasCleared) {
      log(`modeTimerRemove: alarm '${KEYS.MODE_ALARM}' was cleared`);
    } else {
      log({
        text: `modeTimerRemove: alarm '${KEYS.MODE_ALARM}' was NOT cleared`,
        isError: true,
      });
    }
  });

  if (browser.alarms.onAlarm.hasListener(modeTimerSet)) {
    browser.alarms.onAlarm.removeListener(modeTimerSet);
    log(`modeTimerRemove: onAlarm listener 'modeTimerSet' removed`);
  }
};

browser.runtime.onInstalled.addListener(async ({ reason }) => {
  if (reason === 'install') {
    browser.runtime.openOptionsPage();
  } else if (reason === 'update') {
    const url = browser.runtime.getURL('history/history.html');
    browser.tabs
      .create({ url })
      .then((tabWasCreated) => {
        log(
          `options.historyClick: tab created=${JSON.stringify(
            tabWasCreated,
            null,
            2,
          )}`,
        );
      })
      .catch((error) => {
        log({
          isError: true,
          text: `options.historyClick: error while creating tab\n${error}`,
        });
      });
  }
});

browser.runtime.onMessage.addListener((message) => {
  if (
    [
      KEYS.MESSAGE_TYPES.MODE_SET,
      KEYS.MESSAGE_TYPES.LOG,
      KEYS.MESSAGE_TYPES.TIMER_SET,
      KEYS.MESSAGE_TYPES.TIMER_REMOVE,
      KEYS.MESSAGE_TYPES.OPTION_SAVE,
      KEYS.MESSAGE_TYPES.DEBUG_CHANGE,
    ].includes(message.type)
  ) {
    log(
      `background.onMessage: message received: \n${JSON.stringify(
        message,
        null,
        2,
      )}`,
    );
  }

  switch (message.type) {
    case KEYS.MESSAGE_TYPES.MODE_SET:
      modeSet(message.colorMode);
      break;
    case KEYS.MESSAGE_TYPES.LOG:
      log(message.message);
      break;
    case KEYS.MESSAGE_TYPES.TIMER_SET:
      modeTimerSet();
      break;
    case KEYS.MESSAGE_TYPES.TIMER_REMOVE:
      modeTimerRemove();
      break;
    case KEYS.MESSAGE_TYPES.OPTION_SAVE:
      optionSave(message.option);
      break;
    case KEYS.MESSAGE_TYPES.DEBUG_CHANGE:
      debug = message.debug;
      optionSave({ debugIsSet: debug });
      break;
  }
});

browser.runtime.onMessage.addListener((message) => {
  if (
    [KEYS.MESSAGE_TYPES.VALUE_FETCH, KEYS.KEYS_FETCH].includes(message.type)
  ) {
    log(
      `background.onMessage: message received: \n${JSON.stringify(
        message,
        null,
        2,
      )}`,
    );
  }

  if (message.type === KEYS.MESSAGE_TYPES.VALUE_FETCH) {
    return optionFetch(message.optionKey)
      .then((option) => {
        const value = option[message.optionKey];
        return Promise.resolve(value);
      })
      .catch((error) => {
        const message = `onMessage: error fetching value\n${error}`;
        log({ text: message, isError: true });
        return Promise.reject(new Error(message));
      });
  }

  if (message.type === KEYS.KEYS_FETCH) {
    return Promise.resolve(KEYS);
  }
});

browser.storage.onChanged.addListener((changes, areaName) => {
  if (areaName !== 'sync') return;
  if (!changes.colorMode) return;
  if (changes.colorMode.newValue === changes.colorMode.oldValue) return;

  const mode = changes.colorMode.newValue;
  modeSet(mode);

  browser.runtime
    .sendMessage({
      type: KEYS.MESSAGE_TYPES.MODE_SET,
      colorMode: mode,
    })
    .then((response) => {
      log(
        `storageChanged: colorMode changed\n${JSON.stringify(
          changes,
          null,
          2,
        )}`,
      );
      if (response) {
        log(`storageChanged: message response:\n${response}`);
      }
    })
    .catch((error) => {
      log({
        isError: true,
        text: `storageChanged: error sending message (no other pages are open right now)\n${error}`,
      });
    });
});

optionFetch(null).then((options) => {
  const { colorMode, debugIsSet, lightHours, scheduleIsSet } = options;
  if (colorMode) {
    modeSet(colorMode);
  } else {
    modeSet(KEYS.MODE_LIGHT);
    optionSave({ colorMode: KEYS.MODE_LIGHT });
  }

  if (debugIsSet) {
    debug = debugIsSet;
  } else {
    optionSave({ debugIsSet: debug });
  }

  if (!lightHours) {
    optionSave({ lightHours: { start: 6, end: 19 } });
  }

  if (scheduleIsSet) {
    modeTimerSet();
  }
});
