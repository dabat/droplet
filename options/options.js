let KEYS = undefined;
const debugCheckbox = document.getElementById('debug');
const historyButton = document.getElementById('history');

const historyClick = () => {
  const url = browser.runtime.getURL('history/history.html');
  browser.tabs
    .create({ url })
    .then((tabWasCreated) => {
      messageSendAsync({
        type: KEYS.MESSAGE_TYPES.LOG,
        message: `options.historyClick: tab created=${JSON.stringify(
          tabWasCreated,
          null,
          2,
        )}`,
      });
    })
    .catch((error) => {
      messageSendAsync({
        type: KEYS.MESSAGE_TYPES.LOG,
        message: {
          isError: true,
          text: `options.historyClick: error while creating tab\n${error}`,
        },
      });
    });
};

const debugChange = () => {
  messageSendAsync({
    type: KEYS.MESSAGE_TYPES.DEBUG_CHANGE,
    debug: debugCheckbox.checked,
  });
};

const modeApply = (mode) => {
  document.documentElement.setAttribute(KEYS.MODE_ATTRIBUTE, mode);
};

const messageSendAsync = async (message) => {
  const response = await browser.runtime.sendMessage(message);
  return response;
};

const initializeAsync = async () => {
  await messageSendAsync({
    type: 'keysFetch',
  })
    .then((result) => {
      KEYS = result;
      messageSendAsync({
        type: KEYS.MESSAGE_TYPES.LOG,
        message: `options.initializeAsync: KEYS fetched\n${JSON.stringify(
          KEYS,
          null,
          2,
        )}`,
      });
    })
    .catch((error) => {
      messageSendAsync({
        type: 'log',
        message: {
          text: `options.initializeAsync: ERROR fetching KEYS\n${error}`,
          isError: true,
        },
      });
    });

  await browser.storage.sync
    .get(null)
    .then((options) => {
      const { colorMode, debugIsSet } = options;

      document.documentElement.setAttribute(KEYS.MODE_ATTRIBUTE, colorMode);

      debugCheckbox.checked = debugIsSet;

      messageSendAsync({
        type: KEYS.MESSAGE_TYPES.LOG,
        message: `options.initializeAsync: fetched options\n${JSON.stringify(
          options,
          null,
          2,
        )}`,
      });
    })
    .catch((error) => {
      messageSendAsync({
        type: KEYS.MESSAGE_TYPES.LOG,
        message: {
          text: `options.initializeAsync: error fetching saved options\n${error}`,
          isError: true,
        },
      });
    });

  debugCheckbox.addEventListener('change', debugChange);
  historyButton.addEventListener('click', historyClick);
};

browser.runtime.onMessage.addListener((message) => {
  if (message.type === KEYS.MESSAGE_TYPES.MODE_SET) {
    modeApply(message.colorMode);
    messageSendAsync({
      type: KEYS.MESSAGE_TYPES.LOG,
      message: `options.onMessage: message received ${JSON.stringify(
        message,
        null,
        2,
      )}`,
    });
  }
});

initializeAsync();
